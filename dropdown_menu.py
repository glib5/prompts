"""
simple module for tkinter-based UI for retrieve from dropdown menu
"""

from tkinter import Button, Label, LabelFrame, OptionMenu, StringVar, Tk
from typing import Iterable


def __red_cross():
    """what to do when the red cross is clicked"""
    print("Red Cross clicked in a prompt -- quitting program")
    quit()
    # raising exception won't close the prompt, unfortunately
    # can still be done here

# =============================================================================

class _multi_menu(Tk):
    '''class for getting input from a dropdown menu'''
    
    def __init__(self, options, caption): # do NOT modify this function
        super().__init__()
        self.options = options
        self.caption = caption
        self.grid()
        self.root = LabelFrame(self, text='')
        self.root.grid(row=0, columnspan=7, sticky='W')
        self.lab = Label(self.root, text=self.caption)
        self.lab.grid(row=0, column=0, sticky='E', padx=5, pady=2)
        self.choice = StringVar(self)
        self.menu = OptionMenu(self.root, self.choice, *self.options)
        self.menu.grid(row=0, column=1, columnspan=3, pady=2, sticky='WE')
        self.fin = None
        SubmitBtn = Button(self.root, text='Submit', command=self.submit)
        SubmitBtn.grid(row=1, column=3, sticky='W', pady=2)

    def submit(self):
        self.fin = self.choice.get()
        self.root.destroy()
        self.quit()


def get_multi_menu(options, caption:str=None)->str:
    """handles the dropdown menu UI.
    returns empty str if no option is selected"""
    assert isinstance(options, Iterable)
    if caption is None:
        caption = " Select one option "
    assert isinstance(caption, str)
    ui = _multi_menu(options, caption)
    ui.title('')
    ui.protocol("WM_DELETE_WINDOW", __red_cross)
    ui.mainloop()
    fin = ui.fin if ui.fin else ''
    ui.destroy()
    del ui
    return fin 
