from datetime import datetime
import tkinter as tk

from tkcalendar import Calendar


def _red_cross():
    """what to do when the red cross is clicked"""
    print("Red Cross clicked in a prompt -- quitting program")
    quit()
    # raising exception won't close the prompt, unfortunately
    # can still be done here


class _myCalendar(tk.Tk):
    '''class for getting a single date from a nice window UI calendar'''
    
    def __init__(self): # do NOT modify this function
        super().__init__()
        self.withdraw()
        self.date = None # output 
        self.root = tk.Tk()
        #self.root.geometry("350x250") # x and y dimension    
        self.root.title('Pick a Date')
        self.cal = Calendar(self.root, selectmode = 'day')
        self.cal.pack()      
        SubmitBtn = tk.Button(self.root, text='Submit', command=self.submit)
        SubmitBtn.pack() 

    def submit(self):
        self.date = self.cal.get_date()
        self.root.destroy()
        self.quit()


def get_calendar_date() -> datetime:
    """handles the calendar object. returns a datetime object"""   
    C = _myCalendar()
    C.root.protocol("WM_DELETE_WINDOW", _red_cross)
    C.mainloop()
    date = C.date 
    C.destroy()
    del C
    return datetime.strptime(date, '%m/%d/%y')