import tkinter as tk
from tkinter.messagebox import askyesno, showinfo
from tkinter.simpledialog import askstring

# examples

def main():

    # needed!
    tk.Tk().withdraw()

    # message
    showinfo("title", "info")

    # Y/N returns True/False
    a = askyesno(title="title", message="Yes or no?")
    print(a, not a)

    # ask str
    s = askstring(title="title", prompt="give a string")
    print(s)

    return 0

if __name__=='__main__':
    main()
    


