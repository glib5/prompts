"""
simple module for tkinter-based UI
- retrieve multiple SCALAR sinputs
- get calendar date
"""

from datetime import datetime
from keyword import iskeyword
from tkinter import Button, Entry, Label, LabelFrame, Tk
from typing import Dict, Any


def __red_cross():
    """what to do when the red cross is clicked"""
    print("Red Cross clicked in a prompt -- quitting program")
    quit()
    # raising exception won't close the prompt, unfortunately
    # can still be done here


class _multi_inpt(Tk):
    """class for getting multiple inputs from a nice window UI"""

    def __init__(self, names):
        super().__init__()
        self.names = names
        self.inames = tuple("i%s"%str(i) for i in range(len(self.names)))
        self.out = None
        self.title(' ')
        self.minsize(200, 0)
        self.grid()
        self.root = LabelFrame(self, text="Insert values")
        self.root.grid(row=0, columnspan=2)
        for r, (iname, name) in enumerate(zip(self.inames, self.names)):
            # set the label
            setattr(self, name, Label(self.root, text=name))
            getattr(self, name).grid(row=r, column=0)
            # set the input entry line
            setattr(self, iname, Entry(self.root, width=20))
            getattr(self, iname).grid(row=r, column=1)
        SubmitBtn = Button(self.root, text='Submit', command=self.submit)
        SubmitBtn.grid(row=len(names)+1, column=1)

    def submit(self):
        self.out = {n:getattr(self, i).get() for n,i in zip(self.names, self.inames)}
        self.quit()
        

def get_inputs(input_dict: Dict[str:Any]) -> Dict[str:Any]:
    '''
    retreive multiple SCALAR values from basic UI

    Args:
        input_dict: dict of the form {"param_name": param_default_value}

        >>> {"name": "default_name"}

    Returns:
        a dictionary like {"param_name": provided_value}
    '''
    
    assert isinstance(input_dict, dict)
    for k in input_dict.keys():
        assert isinstance(k, str)
        assert not iskeyword(k)

    # retrieve inputs from GUI
    ui = _multi_inpt(input_dict.keys())
    ui.protocol("WM_DELETE_WINDOW", __red_cross)
    ui.mainloop()
    fin = ui.out
    ui.destroy()
    del ui
    # default value handle + input evaluation to correct dtype
    for k, v in input_dict.items():
        if not fin[k]: # if not choosen
            fin[k] = v # -> replace with default
        try: # cast to correct dtype
            t = type(v)
            fin[k] = t(fin[k])
        except ValueError: # provided a str for a numerical input
            fin[k] = v # -> replace with default
    return fin


def get_date() -> datetime:
    """retrives a date from user input"""
    labels = ["year", "month (1-12)", "day (1-31)"]
    cal = _multi_inpt(labels)
    cal.title("Create a date")
    cal.minsize(250, 0)
    cal.protocol("WM_DELETE_WINDOW", __red_cross)
    cal.mainloop()
    fin = cal.out
    cal.destroy()
    del cal
    _year = int(fin[labels[0]])
    _month = int(fin[labels[1]])
    _day = int(fin[labels[2]])
    return datetime(year=_year, month=_month, day=_day)

